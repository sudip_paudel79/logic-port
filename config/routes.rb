Rails.application.routes.draw do

  get 'users/create'
  root 'homes#show'
  devise_for :users
  resource :home, only: :show
  resources :profiles do
    collection do
      get 'showresult'
    end
  end
  resources :sections
  resources :faculties
  resources :batches
  resources :users

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
