class CreateFaculties < ActiveRecord::Migration[5.1]
  def change
    create_table :faculties do |t|
      t.string :facultyname

      t.timestamps
    end
  end
end
