class AddFullnameAddressPhoneNoToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :firstname, :string
    add_column :users, :middlename, :string
    add_column :users, :lastname, :string
    add_column :users, :permanent_address, :string
    add_column :users, :student_phone, :integer
    add_column :users, :temporary_address, :string
    add_column :users, :SEE_grade, :string
    add_column :users, :father_name, :string
    add_column :users, :Mother_name, :string
    add_column :users, :gender, :string
  end

  #def data
  #  User.find_each do |user|
  #    profile=Profile.first
  #    user.update(firstname: profile.firstname, middlename: profile.middlename)
  #  end
  #end
end
