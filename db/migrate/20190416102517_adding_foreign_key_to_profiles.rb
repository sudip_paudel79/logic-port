class AddingForeignKeyToProfiles < ActiveRecord::Migration[5.1]
  def change
    def self.up
    add_foreign_key :Profiles, :batches
    add_foreign_key :Profiles, :sections
    add_foreign_key :Profiles, :faculties
    end
    def self.down
    remove_foreign_key :profiles, :batch_id
    remove_foreign_key :profiles, :secton_id
    remove_foreign_key :profiles, :faculty_id
    end
  end
end
