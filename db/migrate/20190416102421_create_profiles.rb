class CreateProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :profiles do |t|
      t.string :fullname
      t.string :address
      t.bigint :phone_no
      t.string :email
      t.bigint :batch_id
      t.bigint :faculty_id
      t.bigint :section_id

      t.timestamps
    end
  end
end
