class RemoveEmailFromProfiles < ActiveRecord::Migration[5.1]
  def change
    remove_column :profiles, :email
  end
end
