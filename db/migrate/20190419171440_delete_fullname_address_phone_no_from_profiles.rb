class DeleteFullnameAddressPhoneNoFromProfiles < ActiveRecord::Migration[5.1]
  def change
    remove_column :profiles, :fullname
    remove_column :profiles, :phone_no
    remove_column :profiles, :address
  end
end
