class Batch < ApplicationRecord
  has_many :profiles
  has_many :faculties, through: :profiles
end
