class Faculty < ApplicationRecord
  has_many :profiles
  has_many :sections, through: :profiles


end
