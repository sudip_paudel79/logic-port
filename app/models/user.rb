class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :profiles
  has_many :faculties, through: :profiles
  has_many :batches, through: :profiles
  has_many :sections, through: :profiles
  validates :firstname, :lastname ,:permanent_address, :temporary_address,:student_phone, :SEE_grade, :father_name, :Mother_name, :gender, presence: true

  validates :image, attachment_presence: true


  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>", small: "50x50>"}, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  enum role: [:user, :admin]
 
  scope :order_by, ->(sort_by, sort_order) {
    sort_order =
      if sort_order&.downcase == 'desc'
        'desc'
      else
        'asc'
      end
    sort_by =
      if %w(firstname middlename lastname email permanent_address temporary_address).include?(sort_by&.downcase)
        sort_by.downcase
      else
        'firstname'
      end
    order(sort_by.to_sym => sort_order.to_sym)
  }

  scope :filter_by_batch_id, ->(batch_id) {
    if batch_id.present?
      includes(:profiles)
        .where(profiles: {batch_id:  batch_id})
    end
  }

  scope :filter_by_section_id, ->(section_id) {
    includes(:profiles)
      .where(profiles: {section_id:  section_id}) if section_id.present?
  }

  scope :filter_by_faculty_id, ->(faculty_id) {
    includes(:profiles)
      .where(profiles: {faculty_id:  faculty_id}) if faculty_id.present?
  }
end
