// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/


// document.addEventListener 'DOMContentLoaded', ->
//names = document.querySelectorAll '.name'
//names.forEach (name) ->
//name.addEventListener 'click', (e)->
//alert 'name clicked'
//e.preventDefault()

$(document).ready(function(){  // for searching by batch faculty and sections
  var filterItems = document.querySelectorAll('.select-form');
  var sortFields = document.querySelectorAll('.sort');
  // var profileList = document.getElementById("profileList")
  // if (profileList) {
  //   var showProfile = profileList.getElementsByTagName('tr');
  // } else {
  //   var showProfile = []
  // }
  // var closeProfile = document.getElementById("closedetail")
  filterItems.forEach(function(filterItem) {
    filterItem.addEventListener('change', filterProfiles)
  })
  
  sortFields.forEach(function(sortField){  
    sortField.addEventListener('click', function() {
      sortProfiles(sortField)
    })
  })

  $('#profileList td span').click(function(){
    $('#profileTable').slideToggle()
    $('#profileList td span').toggleClass('rotate-icon')
  });

  $('table .profileList tr').click(function(){
    $(this.dataset.target).slideToggle()
  });

  // $('#profileList tr').click(function(){
  //   $('#profileTable').slideToggle()
  // })
  // $('#profileList tr').click(displayProfile)
  
  // for(i=0; i<showProfile.length;i++){
  //   showProfile[i].addEventListener('click', displayProfile);
  // }
  // closeProfile.addEventListener('click', closeProfile)

  // MAIN FUNCTIONS
  sortQueries = function() {
    var sort = []
    sortFields.forEach(function(sortQuery) {
      var sortOrder = sortQuery.getAttribute('data-sort_order')
      if (sortOrder) {
        var sortBy = sortQuery.getAttribute('data-sort_by')
        sort = [`sort_by=${sortBy}`, `sort_order=${sortOrder}`]
      }
    })
    return sort
  }

  function searchQueries() {
    var searches = []
    filterItems.forEach(function(filterItem) {
      if (filterItem.value){
        searches.push(`${filterItem.name}=${filterItem.value}`)
      }
    })
    return searches
  }
  
  function filterProfiles(){
    var searches = searchQueries()
    var sort = sortQueries()
    location = [(location.origin + location.pathname), searches.concat(sort).join('&')].join('?')
  }
  
  function sortProfiles(sortField) {
    var sortOrder = '';
    if (sortField.getAttribute('data-sort_order') == 'asc') {
      sortOrder = 'sort_order=desc'
    } else {
      sortOrder = 'sort_order=asc'
    }
    var sortBy = `sort_by=${sortField.getAttribute('data-sort_by')}`
    var searches = searchQueries()
    location = [(location.origin + location.pathname), searches.concat([sortBy, sortOrder]).join('&')].join('?')
  }

  // function displayProfile(){
  //   var list = [];
  //   var footerData = [];
  //   var childNodes = this.childNodes;
  //   var i = 1;
  //   while( i < childNodes.length - 6 ){
  //     list.push(`${childNodes[i].getAttribute('data-name')}: ${this.childNodes[i].innerHTML}`)
  //     i=i+2;
  //   }
  //   var j = 23;
  //   while ( j < childNodes.length){
  //     footerData.push(childNodes[j].innerHTML);
  //     j+=2;
  //   }

  //   document.getElementById("modalTitle").innerHTML = footerData.join('<br>');
  //   document.getElementById("myModalBody").innerHTML = list.join('<br>');
  //   document.getElementById("modalFooter").innerHTML = footerData.join(" ");
  // }

  function showHideProfileList(){

  }

  // function closeProfile(){
  //   var close = document.getElementById("myModal")
  //   close.classList.remove("visible")
  // } 


  // learnt to use forEach
  // changeFormColor = document.querySelectorAll('.select-form');
  // changeFormColor.forEach(function(changecolor){
  //   changecolor.style.border ='1px solid orange';
  // })




  // for(i=0;i<changeFormColor.length;i++)
  // {
  //   changeFormColor[i].style.border = '1px solid orange';
  // }
   

  //   filterItems.forEach(function(filterItem) {
  //   filterItem.addEventListener('click', () => {alert('helo')} )
  // })
  //myFunction = (e)->
    //alert 'name previewed'
    //e.preventDefault()

  //studentpreview= document.getElementById("studentpreview")
  //studentpreview.addEventListener 'click', myFunction
  // [...buttons].forEach((button) => {
  //   button.addEventListener('click', () => {
  //     console.log("spread forEach worked");
  //   });
});