module ProfilesHelper
  
  def data_sort_order(field_name)
    if params[:sort_by].blank? && field_name == 'firstname'
      'asc'
    elsif params[:sort_by]&.downcase == field_name
      params[:sort_order]
    end
  end
  
end
