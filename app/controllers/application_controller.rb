class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?
  protect_from_forgery with: :exception
  add_flash_types :success, :danger, :info
  # after_action :flash_me

  def after_sign_in_path_for(resource)
    profiles_path
  end

  # def flash_me
  #   flash[:notice] = 'Hello'
  # end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, 
      keys: 
      [:firstname, :middlename, 
        :lastname, :permanent_address, 
        :student_phone, :temporary_address,
        :SEE_grade, :father_name, 
        :Mother_name, :gender, 
        :image, :role
      ])
  end


end
