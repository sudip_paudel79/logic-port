class ProfilesController < ApplicationController
  def index
    if current_user.admin?
      @users =
        User.includes(:faculties,:batches,:sections)
            .filter_by_batch_id(params[:batch_id])
            .filter_by_section_id(params[:section_id])
            .filter_by_faculty_id(params[:faculty_id])
            .order_by(params[:sort_by], params[:sort_order]).page(params[:page]).per(5)
    else
      @faculties = current_user.faculties
      @batches = current_user.batches
      @sections = current_user.sections
      @profiles = current_user.profiles
    end
  end

  def showresult
    @users = User.joins(:profiles).search
  end

  def new
    if current_user.user?
      @profile = current_user.profiles.build
    else
       redirect_to profiles_path, danger:"Admin cannot have any profiles"
    end 
  end

  def create
    @profile = current_user.profiles.build(profile_params)
    respond_to do |format|
      if @profile.save
          format.html{ redirect_to @profile, success:'Profile was successfully created' }
          format.json{render :show, status::created,location: @profile}
      else
        format.html{render :new, danger: @profile.errors.full_messages.to_sentence}
        format.json{render json: @faculty.errors, status: :unprocessable_entity}
      end
    end  
  end

  def show
    @faculties=  current_user.faculties
    @batches= current_user.batches
    @sections= current_user.sections
    if current_user.admin?
      @user = User.find(params[:id])
    end
  end

  def edit
    @profile = Profile.find(params[:id]) 
  end

  def update
    @profile = Profile.find(params[:id])
    if @profile.update(profile_params)
      redirect_to profiles_path, success: 'Profile is successfully updated.'
    else
      render :edit, danger: 'Profile wasnot updated'
    end  
  end

  private
  def profile_params
    params.require(:profile).permit(:faculty_id, :batch_id, :section_id,:user_id)
  end
end
