class HomesController < ApplicationController
 skip_before_action :authenticate_user!, only: :show

  def index
  end

  def new
  end

  def show

  end
end
