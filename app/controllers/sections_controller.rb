class SectionsController < ApplicationController
  def index
    @sections = Section.all
  end

  def new
    if current_user.admin?
      @section = Section.new
    else
      redirect_to sections_path, info: 'Users cannot create new sections.'
    end
  end

  def create
   @section = Section.new(section_args)
   respond_to do |format|
     if @section.save
       format.html {redirect_to @section, notice: 'section was successfully created'}
       format.html {render :show, status: :created, location: @section}
     else
       format.html {render :new}
       format.json { render json: @section.errors, status: :unprocessable_entity }
     end
   end
  end

  def edit
    if current_user.admin?
      @section = Section.find(params[:id])
    else
    redirect_to sections_path, info: 'User cannot update section.'  
    end
  end

  def update
    @section = Section.find(params[:id])
    if @section.update(section_params)
      redirect_to sections_path, info: 'Section is successfully updated.'
    else
      render :edit, danger: 'section wasnot updated'
    end
  end

  def show
    @section = Section.find(params[:id])
  end

  private
  def section_args
   params.require(:section).permit(:section)
  end
end
