class UsersController < ApplicationController
  def index
  end
  def edit
    @user = User.find(params[:id])
  end

  def update
    @user =User.find(params[:id])
    if @user.update(users_params)
      redirect_to profiles_path, success: 'User was successfully updated.'
    else 
      redirect_to :edit, danger: 'User is not updated'
    end
  end

  private

  def users_params
  params.require(:user).permit(:email, :firstname, :middlename, 
        :lastname, :permanent_address, 
        :student_phone, :temporary_address,
        :SEE_grade, :father_name, 
        :Mother_name, :gender, 
        :image, :role)
  end

end
