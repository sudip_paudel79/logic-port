class FacultiesController < ApplicationController
  def index
    @faculties= Faculty.all
  
  end

  def show
    @faculty =  Faculty.find(params[:id])

  end

  def new
    if current_user.admin?
      @faculty=Faculty.new
    else
      redirect_to faculties_path, info: 'User cannot create new Faculty.'
    end
  end


  def create
    @faculty= Faculty.new(faculty_params)
    respond_to do |format|
      if @faculty.save
        format.html { redirect_to @faculty, success: 'Faculty was successfully created.' }
        format.json { render :show, status: :created, location: @faculty }
      else
        format.html { render :new }
        format.json { render json: @faculty.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    if current_user.admin?
      @faculty = Faculty.find(params[:id])
    else
    redirect_to faculties_path, info: 'User cannot update faculty'  
    end
  end

  def update
    @faculty = Faculty.find(params[:id])
    if @faculty.update(faculty_params)
      redirect_to faculties_path, success: 'faculty is successfully updated.'
    else
      render :edit, danger: 'faculty wasnot updated'
    end
  end

  private
  def faculty_params
    params.require(:faculty).permit(:facultyname)
  end
end
