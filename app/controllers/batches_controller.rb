class BatchesController < ApplicationController


  def index
    @batches= Batch.all
  end

  def new
    if current_user.admin?
      @batch= Batch.new
    else
      redirect_to batches_path, danger: 'Users cannot create new Batch.'
    end
  end

  def show
   @batch= Batch.find(params[:id])
  end

  def create
    @batch= Batch.new(batch_params)
    respond_to do |format|
      if @batch.save
        format.html { redirect_to @batch, success: 'Batch was successfully created.' }
        format.json { render :show, status: :created, location: @batch }
      else
        format.html { render :new }
        format.json { render json: @batch.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    if current_user.admin?
      @batch = Batch.find(params[:id])
    else
      redirect_to batches_path, info: 'Users canot update batches.'
    end
  end

  def update
    @batch = Batch.find(params[:id])
    if @batch.update(batch_params)
    redirect_to batches_path, success: 'Batch was successfully updated.' 
    else
      render :edit, danger: 'Batch wasnot updated' 
    end
  end

  private
  def batch_params
   params.require(:batch).permit(:year)
  end
end
